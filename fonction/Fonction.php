<?php
require("config.php");

  function alerte(){
    echo "<script>alert(\"Il manque des informations\")</script>";
  }  
  function getListeType()
  {

    $connexion = connexionBD();
    $type="SELECT * FROM TYPE";
    $stmt=$connexion->prepare($type);
    $stmt->execute();
    return $stmt;

  }

  function getListeGen()
  {

      $connexion = connexionBD();
      $gen="SELECT gen FROM GENERATION";
      $stmt=$connexion->prepare($gen);
      $stmt->execute();
      return $stmt;

  }

  function getListeGenre()
  {

      $connexion = connexionBD();
      $genre="SELECT sexe FROM GENRE";
      $stmt=$connexion->prepare($genre);
      $stmt->execute();
      return $stmt;

  }

  function getListeRegion()
  {

      $connexion = connexionBD();
      $reg="SELECT region FROM REGION";
      $stmt=$connexion->prepare($reg);
      $stmt->execute();
      return $stmt;

  }

  function getListePseudoDresseur()
  {

      $connexion = connexionBD();
      $psd="SELECT pseudoD FROM DRESSEUR";
      $stmt=$connexion->prepare($psd);
      $stmt->execute();
      return $stmt;
  }

  function ajouterPokemon($nom, $surnom, $sexe, $type1, $type2, $gen, $description, $image, $pseudo)
    {

        $connexion = connexionBD();
        $sql = "INSERT INTO POKEMON(idP, nomP, surnomP, sexe, type1, type2, gen, descriptionP, imageP, pseudoD)
                SELECT MAX(idP)+1, :nomP, :surnomP, :sexe, :type1, :type2, :gen, :descriptionP, :imageP, :pseudoD FROM POKEMON";
        $stmt = $connexion->prepare($sql);
        $stmt->bindParam(':nomP', $nom);
        $stmt->bindParam(':surnomP', $surnom);
        $stmt->bindParam(':sexe', $sexe);
        $stmt->bindParam(':type1', $type1);
        $stmt->bindParam(':type2', $type2);
        $stmt->bindParam(':gen', $gen);
        $stmt->bindParam(':descriptionP', $description);
        $stmt->bindParam(':imageP', $image);
        $stmt->bindParam(':pseudoD', $pseudo);
        $stmt->execute();
        echo "<script>alert(\" Votre pokemon a bien été créer\")</script>";


    }
    function ajouterDresseur($pseudo, $sexe, $region, $image)
    {

        $connexion = connexionBD();
        $sql = "INSERT INTO DRESSEUR(pseudoD, sexe, region, imageD)
                VALUES (:pseudoD, :sexe, :region, :imageD)";
        $stmt = $connexion->prepare($sql);
        $stmt->bindParam(':pseudoD', $pseudo);
        $stmt->bindParam(':sexe', $sexe);
        $stmt->bindParam(':region', $region);
        $stmt->bindParam(':imageD', $image);
        $stmt->execute();
        echo "<script>alert(\" Votre dresseur a bien été créer\")</script>";


    }
    function supprimerDresseur($pseudo){
        $connexion = connexionBD();
        $sql="DELETE FROM POKEMON WHERE pseudoD = :pseudoD;
              DELETE FROM DRESSEUR WHERE pseudoD = :pseudoD";
        $stmt = $connexion->prepare($sql);
        $stmt->bindParam(':pseudoD', $pseudo);
        $stmt->execute();
        echo "<script>alert(\" Votre dresseur a bien été éffacé\")</script>";
    }
    function supprimerPokemon($idP){
        $connexion = connexionBD();
        $sql="DELETE FROM POKEMON WHERE idP = :idP ";
        $stmt = $connexion->prepare($sql);
        $stmt->bindParam(':idP', $idP);
        $stmt->execute();
        echo "<script>alert(\" Votre pokemon a bien été éffacé\")</script>";
    }
    function modifierDresseur($pseudo, $sexe, $region, $image){
        $connexion = connexionBD();
        $sql = "UPDATE DRESSEUR SET sexe = :sexe, region = :region, imageD = :imageD WHERE pseudoD=:pseudoD";
        $stmt = $connexion->prepare($sql);
        $stmt->bindParam(':pseudoD', $pseudo);
        $stmt->bindParam(':sexe', $sexe);
        $stmt->bindParam(':region', $region);
        $stmt->bindParam(':imageD', $image);
        $stmt->execute();
        echo "<script>alert(\" Votre dresseur a bien été modifié\")</script>";
    }
    function modifierPokemon($idP, $surnom, $sexe, $type1, $type2, $gen, $description, $image){
        $connexion = connexionBD();
        $sql = "UPDATE POKEMON SET surnomP = :surnomP, sexe= :sexe, descriptionP = :descriptionP, imageP=:imageP WHERE idP = :idP" ;
        $stmt = $connexion->prepare($sql);
        $stmt->bindParam(':surnomP', $surnom);
        $stmt->bindParam(':sexe', $sexe);
        $stmt->bindParam(':descriptionP', $description);
        $stmt->bindParam(':imageP', $image);
        $stmt->bindParam(':idP', $idP);
        $stmt->execute();
        echo "<script>alert(\" Votre pokemon a bien été modifié\")</script>";
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------

    function getDresseur($pseudo){
      $connexion = connexionBD();
      $psd="SELECT pseudoD, imageD, region, sexe FROM DRESSEUR WHERE pseudoD= :pseudoD";
      $stmt=$connexion->prepare($psd);
      $stmt->bindParam(':pseudoD', $pseudo);
      $stmt->execute();
      return $stmt; }

    function getListeDresseur(){
        $connexion = connexionBD();
        $psd="SELECT pseudoD, imageD, region, sexe FROM DRESSEUR";
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
        
    }
    function getPokemonDresseur($pseudo){
        $connexion = connexionBD();
        $psd="SELECT nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON WHERE pseudoD= :pseudoD ";
        $stmt=$connexion->prepare($psd);
        $stmt->bindParam(':pseudoD', $pseudo);
        $stmt->execute();
        return $stmt;

    }
    function getPokemon(){
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON";
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
        
    }

    function getPokemonPrecis($id){
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON WHERE idP= :idP" ;
        $stmt=$connexion->prepare($psd);
        $stmt->bindParam(':idP', $id);
        $stmt->execute();
        return $stmt;
    }
    
    function getPokemonType($type){
        
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON WHERE ";
        for($i = 0; $i < sizeof($type)-1; $i++)
        {
            $psd = $psd."type1='".$type[$i]."' or type2='".$type[$i]."' or ";
        }
        $psd = $psd."type1='".$type[sizeof($type)-1]."' or type2='".$type[sizeof($type)-1]."'";            
        $stmt=$connexion->prepare($psd);
        $stmt->execute();  
        return $stmt;
        
    }
    function getPokemonGen($gen){
        
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON WHERE ";
        for($i = 0; $i < sizeof($gen)-1; $i++)
        {
            $psd = $psd."gen='".$gen[$i]."' or ";
        }
        $psd = $psd."gen ='".$gen[sizeof($gen)-1]."'";            
        $stmt=$connexion->prepare($psd);
        $stmt->execute();  
        return $stmt;
        
    }
    function getPokemonTypeGen($type, $gen){
        
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON WHERE ";
        for($i = 0; $i < sizeof($type)-1; $i++)
        {
            $psd = $psd."type1='".$type[$i]."' or type2='".$type[$i]."' or ";
        }
        $psd = $psd."type1='".$type[sizeof($type)-1]."' or type2='".$type[sizeof($type)-1]."'";
        $psd = $psd." and ";
        for($i = 0; $i < sizeof($gen)-1; $i++)
        {
            $psd = $psd."gen='".$gen[$i]."' or ";
        }
        $psd = $psd."gen='".$gen[sizeof($gen)-1]."'";            
        $stmt=$connexion->prepare($psd);
        $stmt->execute();  
        return $stmt;
        
    }
    function getDresseurGenre($gender){
        $connexion = connexionBD();
        $psd="SELECT pseudoD, imageD, region, sexe FROM DRESSEUR WHERE sexe = :sexe"; 
        $stmt=$connexion->prepare($psd);
        $stmt->bindParam(':sexe', $gender);
        $stmt->execute();
        return $stmt;
    }
    function getDresseurRegion($region){
        $connexion = connexionBD();
        $psd="SELECT pseudoD, imageD, region, sexe FROM DRESSEUR WHERE ";
        for($i = 0; $i < sizeof($region)-1; $i++)
        {
            $psd = $psd."region='".$region[$i]."' or ";
        }
        $psd = $psd."region='".$region[sizeof($region)-1]."'";  
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
    }
    function getDresseurRegionGenre($region, $genre){
        $connexion = connexionBD();
        $psd="SELECT pseudoD, imageD, region, sexe FROM DRESSEUR WHERE ";
        if (!sizeof($region)-1 == 0){
        for($i = 0; $i < sizeof($region)-1; $i++)
        {
            $psd = $psd."region='".$region[$i]."' or ";
        }
        $psd = $psd."region='".$region[sizeof($region)-1]."' and sexe='".$genre."'";}
        else {
            $psd = $psd."region='".$region[0]."' and sexe='".$genre."'"; 
        } 
                 
        $stmt=$connexion->prepare($psd);
        $stmt->execute();  
        return $stmt;
    }
    function rechercheDresseur($str){
        $connexion = connexionBD();
        $psd="SELECT pseudoD, imageD, region, sexe FROM DRESSEUR WHERE pseudoD LIKE '$str%'" ;
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
    }
    function recherchePokemon($str){
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON WHERE nomP LIKE '$str%' or surnomP LIKE '$str%'";
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
    }
    function getPokemonParNom(){
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON ORDER BY nomP";
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
        
    } 
    function getPokemonParSurnom(){
        $connexion = connexionBD();
        $psd="SELECT idP, pseudoD ,nomP, surnomP, sexe, type1, type2,  gen, descriptionP, imageP FROM POKEMON ORDER BY surnomP";
        $stmt=$connexion->prepare($psd);
        $stmt->execute();
        return $stmt;
        
    }

?>
