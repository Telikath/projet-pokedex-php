DROP TABLE POKEMON;
DROP TABLE DRESSEUR;
DROP TABLE GENERATION;
DROP TABLE GENRE;
DROP TABLE REGION;
DROP TABLE TYPE;

-- ------------------------------------------------------------------------------
CREATE TABLE GENERATION (
  gen int(1) NOT NULL,
  constraint GENERATION primary key(gen)
);

CREATE TABLE GENRE (
  sexe Varchar(6) NOT NULL,
  constraint GENRE primary key(sexe)
);

CREATE TABLE REGION (
  region Varchar(7) NOT NULL,
  constraint REGION primary key(region)
);

CREATE TABLE TYPE (
  type Varchar(15) NOT NULL,
  constraint TYPE primary key(type)
);

-- --------------------------------------------------------------------------
CREATE TABLE DRESSEUR (
  pseudoD Varchar(7) NOT NULL,
  sexe Varchar(6),
  region Varchar(7),
  imageD text,


  constraint DRESSEUR primary key(pseudoD),
  constraint clefGenreD foreign key (sexe) references GENRE(sexe),
  constraint clefRegionD foreign key (region) references REGION(region)
);

CREATE TABLE POKEMON (
  idP int(4) NOT NULL,
  nomP Varchar(12),
  surnomP Varchar(12),
  sexe Varchar(6),
  type1 Varchar(15),
  type2 Varchar(15),
  gen int(1),
  descriptionP text,
  imageP text,
  pseudoD Varchar(7),

  constraint POKEMON primary key(idP),
  constraint clefGenerationP foreign key (gen) references GENERATION(gen),
  constraint clefGenreP foreign key (sexe) references GENRE(sexe),
  constraint clefTypeP1 foreign key (type1) references TYPE(type),
  constraint clefTypeP2 foreign key (type2) references TYPE(type),
  constraint clefPseudoD foreign key (pseudoD) references DRESSEUR(pseudoD)
);

-- --------------------------------------------------------------------------
INSERT into GENERATION values (1);
INSERT into GENERATION values (2);
INSERT into GENERATION values (3);
INSERT into GENERATION values (4);
INSERT into GENERATION values (5);
INSERT into GENERATION values (6);
INSERT into GENERATION values (7);
INSERT into GENERATION values (8);

INSERT into GENRE values ('Male');
INSERT into GENRE values ('Female');
INSERT into GENRE values ('Other');

INSERT into REGION values ('Kanto');
INSERT into REGION values ('Johto');
INSERT into REGION values ('Hoenn');
INSERT into REGION values ('Sinnoh');
INSERT into REGION values ('Unis');
INSERT into REGION values ('Kalos');
INSERT into REGION values ('Alola');

INSERT into TYPE values ('Aucun');
INSERT into TYPE values ('Eau');
INSERT into TYPE values ('Feu');
INSERT into TYPE values ('Plante');
INSERT into TYPE values ('Glace');
INSERT into TYPE values ('Fee');
INSERT into TYPE values ('Poison');
INSERT into TYPE values ('Dragon');
INSERT into TYPE values ('Tenebre');
INSERT into TYPE values ('Acier');
INSERT into TYPE values ('Spectre');
INSERT into TYPE values ('Vol');
INSERT into TYPE values ('Normal');
INSERT into TYPE values ('Insecte');
INSERT into TYPE values ('Combat');
INSERT into TYPE values ('Sol');
INSERT into TYPE values ('Roche');
INSERT into TYPE values ('Psy');
INSERT into TYPE values ('Electrique');

INSERT into DRESSEUR values('Ash', 'Male', 'Kanto', 'https://orig00.deviantart.net/06cf/f/2016/191/e/8/ash_ketchum_render_by_tzblacktd-da9k0wb.png');
INSERT into DRESSEUR values('Coco', 'Male', 'Johto', 'https://www.pokepedia.fr/images/thumb/b/b5/Professeur_Chen_LGPLGE.png/200px-Professeur_Chen_LGPLGE.png');
INSERT into DRESSEUR values('Green', 'Female', 'Hoenn', 'https://vignette.wikia.nocookie.net/pokemon/images/8/84/Game_character_leafgreen.png/revision/latest?cb=20110310080402');

INSERT into POKEMON values(1, 'Pikachu', 'Pikachu', 'Male', 'Electrique', 'Aucun', 1, 'Souris jaune', 'https://media.giphy.com/media/kNSeTs31XBZ3G/giphy.gif', 'Ash');
INSERT into POKEMON values(2, 'Amphinobi', 'Sachanobi', 'Male', 'Eau', 'Tenebre', 6, 'NINJA !!!!', 'https://www.pokepedia.fr/images/thumb/4/47/Sachanobi-SL.png/406px-Sachanobi-SL.png', 'Ash');
INSERT into POKEMON values(3, 'Tyranocif', 'Godzilla', 'Male', 'Roche', 'Tenebre', 2, 'Un dino', 'https://www.pokepedia.fr/images/thumb/e/e2/Tyranocif-HGSS.png/550px-Tyranocif-HGSS.png', 'Coco');
INSERT into POKEMON values(4, 'Jungko', 'LeSapin', 'Male', 'Plante', 'Aucun', 3, 'un Conifere', 'https://www.pokepedia.fr/images/thumb/d/d3/M%C3%A9ga-Jungko-ROSA.png/800px-M%C3%A9ga-Jungko-ROSA.png', 'Green');
INSERT into POKEMON values(5, 'Cresselia', 'LaLune', 'Female', 'Psy', 'Aucun', 4, 'Vive la lune', 'https://www.pokepedia.fr/images/thumb/b/bf/Cresselia-DP.png/558px-Cresselia-DP.png', 'Coco');
INSERT into POKEMON values(6, 'Papilusion', 'machin', 'Female', 'Insecte', 'Vol', 1, 'le PIIIIIIIIRE !!!!!', 'https://www.pokepedia.fr/images/thumb/8/83/Papilusion-RFVF.png/659px-Papilusion-RFVF.png', 'Coco');
INSERT into POKEMON values(7, 'MELTAN', 'KIRICROU', 'Other', 'Acier', 'Aucun', 8, 'clÃ© Ã  molette', 'https://www.pokepedia.fr/images/thumb/5/57/Meltan-LGPE.png/250px-Meltan-LGPE.png', 'Coco');
