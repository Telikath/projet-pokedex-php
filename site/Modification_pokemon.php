<?php
    require("./../fonction/Fonction.php");
    require("./../fonction/Header.php");
?>
<!DOCTYPE html>
     <body>
        <div id="FM">
            <form id='frm' action="" method="POST">
              <?php $pkmn = getPokemonPrecis($_POST["idP"]);
              $pkmn = $pkmn->fetch();
              echo "
              <div><input id='hid' type='text' name='idP' maxlength='2' value='".$pkmn['idP']."' required></div>
              <div><label>Nom du pokemon :  ".$pkmn['nomP']."</label></div>
                    <div><label> Surnom : </label> <input type=\'text' name='surname' maxlength='12' value='".$pkmn['surnomP']."' required> </div>
                    <div><label> Sexe du pokemon : </label>";

                          $stmt = getListeGenre();
                          while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
                            if ($row['sexe'] == $pkmn['sexe']){
                              echo "<input type='radio' name='gender' value=".$row['sexe']." checked >".$row['sexe'];
                              }
                              else{
                              echo "<input type='radio' name='gender' value=".$row['sexe'].">".$row['sexe'];
                              }
                          }
                          unset($row);?>
                   </div>
                    <?php echo "<div><label>Génération : ".$pkmn['gen']."</label></div>";?>
                    
                    <?php echo "<div><label>Type : ".$pkmn['type1'];
                          if ($pkmn['type2'] != 'Aucun'){
                            echo ", ".$pkmn['type2']."</div>";
                          }
                          else {
                            echo "</div>";
                          }
                          ?>

                    <?php
                    echo "<div><label> Ce pokemon appartient à : ".$pkmn['pseudoD']."</label>
                    <div><label> Description : </label> <textarea type='text' name='description' maxlength='151' style='width:200px; height:100px' >".$pkmn['descriptionP']."</textarea> </div>
                    <div><label>Image :</label>  <input type='text' name='image' style='width:200px' value='".$pkmn['imageP']."'></div>"; ?>

                    <?php
                    
                     if (isset($_POST['bt'])){
                          modifierPokemon($pkmn['idP'],$_POST['surname'],$_POST['gender'],$pkmn['type1'],$pkmn['type2'],$pkmn['gen'],$_POST['description'],$_POST['image']);
                      } ?>
                      <article>
                        <input id='btT' name='bt' type='submit' value='Modifier'>
                      </article>
        </form>
      </div>
 </body>
</html>
