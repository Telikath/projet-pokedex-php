<?php
    require("./../fonction/Fonction.php");
    require("./../fonction/Header.php");
?>

<!DOCTYPE html>
    <body>
    <form action="" method="POST">
    <aside>
    <?php
      echo "<p> Liste des Types";
         $stmt = getListeType();
              while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
            if ($row["type"] != 'Aucun'){
             echo "<div><input type='checkbox' name='typelist[]' value=".$row['type']."><img id='imgT' src='./../image/type/".$row["type"].".png'></div>";}
           }
         unset($row);
      echo "</p>";
      echo "<p> Liste des generations";
         $stmt = getListeGen();
          while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
          echo "<div><input type='checkbox' name='genlist[]' value=".$row['gen'].">".$row['gen']."</div>";
        }
      unset($row);
      echo "</p>";
       ?>
      <input id='btT' name='btT' type='submit' value='Submit' >
      <input name='btN' type='submit' value='Trier par Nom' >
      <input name='btS' type='submit' value='Trier par Surnom' >
    </aside>
    </form>
     <?php
        if (isset($_POST['btS'])){
          $pkmn = getPokemonParSurnom();
          foreach($pkmn as $poke)
          {
         echo "<div><ul id='TC'>
           <li>
           <div>
            <img id='IM' src=".$poke["imageP"].">
             <div id='d'>
                <div> Dresseur : ".$poke["pseudoD"]."</div>
                <div> Nom : ".$poke["nomP"]."</div>
                <div> Surnom : ".$poke["surnomP"]."</div>
                <div> Genre : ".$poke["sexe"]."</div>
                <div> Generation : ".$poke["gen"]."</div>
                <div><img id='imgT' src='./../image/type/".$poke["type1"].".png'></div>";
                if ($poke["type2"]!="Aucun") {
                  echo "<div><img id='imgT' src='./../image/type/".$poke["type2"].".png'></div>";
                }
                echo "<div> Desc : ".$poke["descriptionP"]."</div>
                <form action=\"\" method=\"POST\">
                <button name=\"Supprimer\" value='".$poke['idP']."'>Supprimer</button>
                </form>
               <form action=\"Modification_pokemon.php\" method=\"POST\">
                  <input id='hid' type=\"text\" name=\"idP\" value='".$poke['idP']."'>
                  <input type=\"submit\" name=\"Modifier\" value='Modifier'>
                </form>
            </div>
            </div>
         </li>
         </ul>
         </div>
       ";
              }
         }
        else if (isset($_POST['btN'])){
          $pkmn = getPokemonParNom();
          foreach($pkmn as $poke)
          {
            echo "<div><ul id='TC'>
            <li>
            <div>
             <img id='IM' src=".$poke["imageP"].">
              <div id='d'>
                 <div> Dresseur : ".$poke["pseudoD"]."</div>
                 <div> Nom : ".$poke["nomP"]."</div>
                 <div> Surnom : ".$poke["surnomP"]."</div>
                 <div> Genre : ".$poke["sexe"]."</div>
                 <div> Generation : ".$poke["gen"]."</div>
                 <div><img id='imgT' src='./../image/type/".$poke["type1"].".png'></div>";
                 if ($poke["type2"]!="Aucun") {
                   echo "<div><img id='imgT' src='./../image/type/".$poke["type2"].".png'></div>";
                 }
                 echo "<div> Desc : ".$poke["descriptionP"]."</div>
                 <form action=\"\" method=\"POST\">
                 <button name=\"Supprimer\" value='".$poke['idP']."'>Supprimer</button>
                 </form>
                <form action=\"Modification_pokemon.php\" method=\"POST\">
                   <input id='hid' type=\"text\" name=\"idP\" value='".$poke['idP']."'>
                   <input type=\"submit\" name=\"Modifier\" value='Modifier'>
                 </form>
             </div>
             </div>
          </li>
          </ul>
          </div>
        ";
              }
         }
       else if (isset($_POST['btT'])){
         if (empty($_POST['typelist']) AND empty($_POST['genlist'])){
          echo "<script>alert(\"Impossible\")</script>";
         }
        else{

              if (empty($_POST['typelist']) AND !empty($_POST['genlist'])){
                $pkmn = getPokemonGen($_POST['genlist']);
              }
              else if (!empty($_POST['typelist']) AND empty($_POST['genlist'])){
                $pkmn = getPokemonType($_POST['typelist']);
              }
              if (!empty($_POST['typelist']) AND !empty($_POST['genlist'])){
                $pkmn = getPokemonTypeGen($_POST['typelist'],$_POST['genlist']);
              }
        foreach($pkmn as $poke)
        {
          echo "<div><ul id='TC'>
          <li>
          <div>
           <img id='IM' src=".$poke["imageP"].">
            <div id='d'>
               <div> Dresseur : ".$poke["pseudoD"]."</div>
               <div> Nom : ".$poke["nomP"]."</div>
               <div> Surnom : ".$poke["surnomP"]."</div>
               <div> Genre : ".$poke["sexe"]."</div>
               <div> Generation : ".$poke["gen"]."</div>
               <div><img id='imgT' src='./../image/type/".$poke["type1"].".png'></div>";
               if ($poke["type2"]!="Aucun") {
                 echo "<div><img id='imgT' src='./../image/type/".$poke["type2"].".png'></div>";
               }
               echo "<div> Desc : ".$poke["descriptionP"]."</div>
               <form action=\"\" method=\"POST\">
               <button name=\"Supprimer\" value='".$poke['idP']."'>Supprimer</button>
               </form>
              <form action=\"Modification_pokemon.php\" method=\"POST\">
                 <input id='hid' type=\"text\" name=\"idP\" value='".$poke['idP']."'>
                 <input type=\"submit\" name=\"Modifier\" value='Modifier'>
               </form>
           </div>
           </div>
        </li>
        </ul>
        </div>
      ";
            }}
       }
      else{
     $pkmn = getPokemon();
     foreach ($pkmn as $poke) {
      echo "<div><ul id='TC'>
      <li>
      <div>
       <img id='IM' src=".$poke["imageP"].">
        <div id='d'>
           <div> Dresseur : ".$poke["pseudoD"]."</div>
           <div> Nom : ".$poke["nomP"]."</div>
           <div> Surnom : ".$poke["surnomP"]."</div>
           <div> Genre : ".$poke["sexe"]."</div>
           <div> Generation : ".$poke["gen"]."</div>
           <div><img id='imgT' src='./../image/type/".$poke["type1"].".png'></div>";
           if ($poke["type2"]!="Aucun") {
             echo "<div><img id='imgT' src='./../image/type/".$poke["type2"].".png'></div>";
           }
           echo "<div> Desc : ".$poke["descriptionP"]."</div>
           <form action='' method=\"POST\">
           <button name=\"Supprimer\" value='".$poke['idP']."'>Supprimer</button>
           </form>
          <form action=\"Modification_pokemon.php\" method=\"POST\">
             <input id='hid' type=\"text\" name=\"idP\" value='".$poke['idP']."'>
             <input type=\"submit\" name=\"Modifier\" value='Modifier'>
           </form>
       </div>
       </div>
    </li>
    </ul>
    </div>
  ";}}
      if(isset($_POST['Supprimer']))
      {
        supprimerPokemon($_POST['Supprimer']);
      }
     ?>

   <body>
</html>
