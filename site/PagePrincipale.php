<?php
    require("./../fonction/Fonction.php");
    require("./../fonction/Header.php");
?>


<!DOCTYPE html>
     <body>
        <div id="FM">
            <p>Ce site est la representation d'une base de donnée qui regroupe les dresseurs et leurs pokemons.</p>
            <p>Grace aux outils qui sont mis a votre disposition vous pourrez :</p>
            <ul>
                <li>Créer un pokemon ou un dresseur </li>
                <li>Modifier un pokemon ou un dresseur</li>
                <li>Supprimer un pokemon ou un dresseur</li>
                <li>Trier, filtrer ou rechercher</li>
                <li>Visualiser les dresseur ou les pokemons</li>
            </ul>
      </div>
 </body>
</html>
