<?php
    require("./../fonction/Fonction.php");
    require("./../fonction/Header.php");

?>


<!DOCTYPE html>
     <body>
        <div id="FM">
            <form id='frm' action="" method="POST">

                    <div><label> Nom du pokemon : </label> <input type="text" name="name" maxlength="12" placeholder="Tyranitar" required> </div>
                    <div><label> Surnom du pokemon : </label> <input type="text" name="surname" maxlength="12" placeholder="Boby" required> </div>
                    <div><label> Sexe du pokemon : </label>
                    <?php
                          $stmt = getListeGenre();
                          while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
                            echo "<input type='radio' name='gender' value=".$row['sexe'].">".$row['sexe'];

                          }
                          unset($row);?>

                   </div>
                    <div><label>Génération :</label><select name="gen">
                    <?php
                          $stmt = getListeGen();
                          while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
                            echo "<option value=".$row['gen'].">".$row['gen']."</option>";
                          }
                          unset($row);?>
                              </select>
                    </div>
                    <div><label>Type principale : </label><select name="type1">
                                <?php
                                $stmt = getListeType();
                                while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
                                  if ($row['type']=='Aucun'){
                                    echo "<option value=".$row['type']." Selected >".$row['type']."</option>";
                                  }
                                  else{  
                                  echo "<option value=".$row['type'].">".$row['type']."</option>";
                                  }  
                                }
                                unset($row);
                                ?>
                        </select></div>
                        <div><label>Type secondaire : </label><select name="type2">
                    <?php
                                $stmt = getListeType();
                                while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
                                  if ($row['type']=='Aucun'){
                                    echo "<option value=".$row['type']." Selected >".$row['type']."</option>";
                                  }
                                  else{  
                                  echo "<option value=".$row['type'].">".$row['type']."</option>";
                                  }  
                                }
                                unset($row);
                                ?>
                    </select></div>
                    <div><label> Ce pokemon appartient à : </label> </label><select name="dresseur">
                    <?php
                                $stmt = getListePseudoDresseur();
                                while($row = $stmt -> fetch(PDO::FETCH_ASSOC)){
                                  echo "<option value=".$row['pseudoD'].">".$row['pseudoD']."</option>";
                                }
                                unset($row);
                                ?>
                                </select>
                    <div><label> Description : </label> <textarea type="text" name="description" maxlength="151" style="width:200px; height:100px" ></textarea> </div>
                    <div><label>Image :</label>  <input type="text" name="image" style="width:200px" placeholder="image url"></div>

                    <?php
                    $ajoutP = NULL;
                    
                      if (isset($_POST['bt'])){
                        if($_POST['type1']=='Aucun' or !isset($_POST['gender'])) {
                                alerte();}
                        else{
                        $ajoutP = ajouterPokemon($_POST['name'],$_POST['surname'],$_POST['gender'],$_POST['type1'],$_POST['type2'],$_POST['gen'],$_POST['description'],$_POST['image'],$_POST['dresseur']);
                      }
                    }
                    
                      ?>
                    <article>

                      <input id="btT" name="bt" type="submit" value="Submit" onclick="$ajoutP">
                      <input id="btT" type="reset" onclick="alert('Bien effacé')">
                    </article>
        </form>
      </div>
 </body>
</html>
