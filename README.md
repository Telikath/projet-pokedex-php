Ce site est la representation d'une base de donnée qui regroupe les dresseurs et leurs pokemons.
Grace aux outils qui sont mis a votre disposition vous pourrez :
- Créer un pokemon ou un dresseur
- Modifier un pokemon ou un dresseur
- Supprimer un pokemon ou un dresseur
- Trier, filtrer ou rechercher
- Visualiser les dresseur ou les pokemons
                
Les modifications :
- Le css et certain élément graphique revue
- Les tris sont disponible
- supprimer pokemon / dresseur sont parfaitement fonctionelle
- modifier pokemon / dresseur sont parfaitement fonctionelle
- Page speciale pour les modification ajouté
- fonctionalité voir les pokemon d'un dresseur fonctionnelle
- barre de recherche fonctionnelle
- Page principale ajouté
- restructuration des fichiers